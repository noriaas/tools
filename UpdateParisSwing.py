from webbot import Browser 
import requests
import time
from bs4 import BeautifulSoup
from html.parser import HTMLParser
import os
import regex
import re
import configparser
UpdateConfig = configparser.RawConfigParser()
UpdateConfig.read('UpdateParisConfig.properties')
Jenkins_url_vanbreda=UpdateConfig.get('Jenkins', 'Jenkins.JobURL')
print(Jenkins_url_vanbreda)
ftpeuk_url_vanbreda=UpdateConfig.get('ftp', 'ftp.BranchURL')
print(ftpeuk_url_vanbreda)
customer_parameter=UpdateConfig.get('Jenkins', 'Jenkins.buildApiVanbreda')
print(customer_parameter)
# Jenkins_url_vanbreda='http://10.1.1.8:8080/job/dev_develop/job/dev%252F4.7/'
# ftpeuk_url_vanbreda='http://ftpeuk:krystall@internal.noria.no/ftpeuk/patch/4.7/development/'
# ##########################login paris#################################
web = Browser()
web.go_to(Jenkins_url_vanbreda) 
web.click(tag='div', classname='task', number=4) #build with parameters
web.click(tag='div', classname='setting-main', number=2) #select build type
web.click(text='Deploy', tag='option')
#web.click(tag='div', classname='setting-main', number=11)#select vanbreda
web.click(xpath="//input[@type='checkbox']", number=customer_parameter)
web.click(text='Build', tag='button', id='yui-gen1-button')#click build button
time.sleep(330)
# web.refresh()
web.click(tag='div', classname='build-icon', number=1)
html=web.get_page_source()
soup = BeautifulSoup(html,features="html.parser")
for span in soup.find_all('span'):
    if '.zip' in span.text:
        print("Found the build serial number")
        result = re.search('paris-swing-client/build/distributions/(.*) Paris4.7Dev', span.text)
        build_serial=result.group(1)
        split_string = build_serial.split(".", 1)
        build_serial_number = split_string[0]
        print('build serial number is : ', build_serial_number)    
web.new_tab(ftpeuk_url_vanbreda) 
page=web.get_current_window_handle()
print(page)
time.sleep(3)
tabs=web.get_total_tabs()
print(tabs)
web.press(web.Key.ENTER)
web.new_tab(ftpeuk_url_vanbreda)
time.sleep(2)
cwd = os.getcwd()
print("Current working directory: {0}".format(cwd)) 
ParisDir=UpdateConfig.get('localDir', 'Dir.ParisAPI')
os.chdir(ParisDir)
changedwd=os.getcwd()  
print("changed working directory: {0}".format(changedwd)) 
os.system(f"RunParis.bat {build_serial_number} {ftpeuk_url_vanbreda}")      

