

import time

import os
import regex
import re
import jenkins
# ##########################jenkins#################################
JENKINS_URL = "http://10.1.1.8:8080"
JENKINS_USERNAME = "noria"
JENKINS_PASSWORD = "ntEZu8j4FQFLa8s"
class ServerJenkins:
    def __init__(self):
        self.jenkins_server = jenkins.Jenkins(JENKINS_URL, username=JENKINS_USERNAME, password=JENKINS_PASSWORD)
        user = self.jenkins_server.get_whoami()
        version = self.jenkins_server.get_version()
        print ("Jenkins Version: {}".format(version))
        print ("Jenkins User: {}".format(user['id']))

    def build_job(self, name, parameters=None, token=None):
        next_build_number = self.jenkins_server.get_job_info(name)['nextBuildNumber']
        self.jenkins_server.build_job(name, parameters=parameters, token=token)
        time.sleep(10)
        build_info = self.jenkins_server.get_build_info(name, next_build_number)
        return build_info
    def get_job_info(self, name, depth=0, fetch_all_builds=False):
        job_info=self.jenkins_server.get_job_info(name, depth=0, fetch_all_builds=False)
        return job_info
    def get_build_console_output(self, name, number):
        consol_output=self.jenkins_server.get_build_console_output(name, number)
        return consol_output

if __name__ == "__main__":
    NAME_OF_JOB = "dev_develop/dev%2F4.7""dev_develop/dev%2F4.7"
    TOKEN_NAME = "11dda29f788b8bb1018d9efa9648d7d13d"
    # PARAMETERS = [{'_class': 'hudson.model.BooleanParameterValue', 'name': 'cleanWorkspace', 'value': False}, {'_class': 'hudson.model.StringParameterValue', 'name': 'buildType', 'value': 'Deploy'}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApi', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiDnk', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiGard', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiMoretrygd', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiFremtind', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiLoadSure', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiHeineken', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiOf', 'value': False}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'buildApiVanbreda', 'value': True}, {'_class': 'hudson.model.BooleanParameterValue', 'name': 'gardFinalBuildOfSprint', 'value': False}]
    PARAMETERS = {'buildType': 'Deploy', 'buildApiVanbreda': 'Ture'}
    jenkins_obj = ServerJenkins()
    output = jenkins_obj.build_job("dev_develop/dev%2F4.7", PARAMETERS, {'token': TOKEN_NAME})
    time.sleep(420)
    job_information=jenkins_obj.get_job_info('dev_develop/dev%2F4.7', depth=0, fetch_all_builds=False)
    print('###################')
    build_number=job_information['builds'][0]["number"]
    print('latest build number is: ',build_number)
    print('#############')
    consol=jenkins_obj.get_build_console_output('dev_develop/dev%2F4.7',build_number)
    result = re.search('paris-swing-client/build/distributions/(.*) Paris4.7Dev', consol)
    build_serial=result.group(1)
    split_string = build_serial.split(".", 1)
    build_serial_number = split_string[0]
    print('build serial number is : ', build_serial_number)
    print ("Jenkins Build URL: {}".format(output['url']))

  